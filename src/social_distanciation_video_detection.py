from bird_view_transfo_functions import compute_perspective_transform,compute_point_perspective_transformation
from tf_model_object_detection import Model
from colors import bcolors
import numpy as np
import itertools
import imutils
import time
import math
import glob
import yaml
import cv2
import os

COLOR_RED = (0, 0, 255)
COLOR_GREEN = (0, 255, 0)
COLOR_BLUE = (255, 0, 0)
BIG_CIRCLE = 60
SMALL_CIRCLE = 3


def get_human_box_detection(boxes,scores,classes,height,width):
	"""
	For each object detected, check if it is a human and if the confidence >> our threshold.
	Return 2 coordonates necessary to build the box.
	@ boxes : all our boxes coordinates
	@ scores : confidence score on how good the prediction is -> between 0 & 1
	@ classes : the class of the detected object ( 1 for human )
	@ height : of the image -> to get the real pixel value
	@ width : of the image -> to get the real pixel value
	"""
	array_boxes = list() # Create an empty list
	for i in range(boxes.shape[1]):
		# If the class of the detected object is 1 and the confidence of the prediction is > 0.6
		if int(classes[i]) == 1 and scores[i] > 0.75:
			# Multiply the X coordonnate by the height of the image and the Y coordonate by the width
			# To transform the box value into pixel coordonate values.
			box = [boxes[0,i,0],boxes[0,i,1],boxes[0,i,2],boxes[0,i,3]] * np.array([height, width, height, width])
			# Add the results converted to int
			array_boxes.append((int(box[0]),int(box[1]),int(box[2]),int(box[3])))
	return array_boxes


def get_centroids_and_groundpoints(array_boxes_detected):
	"""
	For every bounding box, compute the centroid and the point located on the bottom center of the box
	@ array_boxes_detected : list containing all our bounding boxes
	"""
	array_centroids,array_groundpoints = [],[] # Initialize empty centroid and ground point lists
	for index,box in enumerate(array_boxes_detected):
		# Draw the bounding box
		# c
		# Get the both important points
		centroid,ground_point = get_points_from_box(box)
		array_centroids.append(centroid)
		array_groundpoints.append(centroid)
	return array_centroids,array_groundpoints


def get_points_from_box(box):
	"""
	Get the center of the bounding and the point "on the ground"
	@ param = box : 2 points representing the bounding box
	@ return = centroid (x1,y1) and ground point (x2,y2)
	"""
	# Center of the box x = (x1+x2)/2 et y = (y1+y2)/2
	center_x = int(((box[1]+box[3])/2))
	center_y = int(((box[0]+box[2])/2))
	# Coordiniate on the point at the bottom center of the box
	center_y_ground = center_y + ((box[2] - box[0])/2)
	return (center_x,center_y),(center_x,int(center_y_ground))


def change_color_on_topview(pair):
	"""
	Draw red circles for the designated pair of points
	"""
	cv2.circle(bird_view_img, (pair[0][0],pair[0][1]), BIG_CIRCLE, COLOR_RED, 2)
	cv2.circle(bird_view_img, (pair[0][0],pair[0][1]), SMALL_CIRCLE, COLOR_RED, -1)
	cv2.circle(bird_view_img, (pair[1][0],pair[1][1]), BIG_CIRCLE, COLOR_RED, 2)
	cv2.circle(bird_view_img, (pair[1][0],pair[1][1]), SMALL_CIRCLE, COLOR_RED, -1)

def draw_rectangle(corner_points):
	# Draw rectangle box over the delimitation area
	cv2.line(frame, (corner_points[0][0], corner_points[0][1]), (corner_points[1][0], corner_points[1][1]), COLOR_BLUE, thickness=1)
	cv2.line(frame, (corner_points[1][0], corner_points[1][1]), (corner_points[3][0], corner_points[3][1]), COLOR_BLUE, thickness=1)
	cv2.line(frame, (corner_points[0][0], corner_points[0][1]), (corner_points[2][0], corner_points[2][1]), COLOR_BLUE, thickness=1)
	cv2.line(frame, (corner_points[3][0], corner_points[3][1]), (corner_points[2][0], corner_points[2][1]), COLOR_BLUE, thickness=1)


def get_social_distance(video_path=None,distance_minimum=50):
    if not video_path:
        print("Please set video path")
        return {}
    vcap = cv2.VideoCapture(video_path)
    if vcap.isOpened():
        width = int(vcap.get(3))  # float
        height = int(vcap.get(4))  # float


    vcap.set(cv2.CAP_PROP_POS_MSEC, 1000)  # just cue to 20 sec. position
    success, image = vcap.read()
    img_path = "../img/static_frame_from_video.jpg"
    if not success:
        print("Video read failed")
        return {}
    else:
        cv2.imwrite(img_path,image)
    corner_points = []
    corner_points.append([0,0])
    corner_points.append([0, height])
    corner_points.append([width,0])
    corner_points.append([width,height])
    width_og = int(width)
    height_og = int(height)
    size_frame = int(height)
    print(bcolors.OKGREEN +" Done : [ Config file loaded ] ..."+bcolors.ENDC )

    model_path="../models/faster_rcnn_inception_v2_coco_2018_01_28/frozen_inference_graph.pb"

    print(bcolors.WARNING + " [ Loading the TENSORFLOW MODEL ... ]"+bcolors.ENDC)
    model = Model(model_path)
    print(bcolors.OKGREEN +"Done : [ Model loaded and initialized ] ..."+bcolors.ENDC)

    matrix,imgOutput = compute_perspective_transform(corner_points,width_og,height_og,cv2.imread(img_path))
    height,width,_ = imgOutput.shape
    blank_image = np.zeros((height,width,3), np.uint8)
    height = blank_image.shape[0]
    width = blank_image.shape[1]
    dim = (width, height)


    vs = cv2.VideoCapture(video_path)
    output_video_1,output_video_2 = None,None
    frameRate = vs.get(5)
    # Loop until the end of the video stream
    result = {}
    frame_num = 1
    time_sec = 0
    while True:

        # Load the image of the ground and resize it to the correct size
        img = cv2.imread("../img/static_frame_from_video.jpg")
        global bird_view_img
        global frame_exists
        global frame
        bird_view_img = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)

        # Load the frame
        (frame_exists, frame) = vs.read()




        # Test if it has reached the end of the video
        if not frame_exists:
            break
        else:
            # Resize the image to the correct size
            frame = imutils.resize(frame, width=int(size_frame))

            # Make the predictions for this frame
            (boxes, scores, classes) =  model.predict(frame)

            # Get the human detected in the frame and return the 2 points to build the bounding box
            array_boxes_detected = get_human_box_detection(boxes,scores[0].tolist(),classes[0].tolist(),frame.shape[0],frame.shape[1])

            # Both of our lists that will contain the centroïds coordonates and the ground points
            array_centroids,array_groundpoints = get_centroids_and_groundpoints(array_boxes_detected)

            # Use the transform matrix to get the transformed coordonates
            transformed_downoids = compute_point_perspective_transformation(matrix,array_groundpoints)

            # Show every point on the top view image
            for point in transformed_downoids:
                x,y = point
                cv2.circle(bird_view_img, (x,y), BIG_CIRCLE, COLOR_GREEN, 2)
                cv2.circle(bird_view_img, (x,y), SMALL_CIRCLE, COLOR_GREEN, -1)
            current_sec = 0
            # Check if 2 or more people have been detected (otherwise no need to detect)
            cv2.imwrite("../output/final-o.jpg", frame)
            if len(transformed_downoids):
                for index,downoid in enumerate(transformed_downoids):
                    if not (downoid[0] > width or downoid[0] < 0 or downoid[1] > height+200 or downoid[1] < 0 ):
                        cv2.rectangle(frame,(array_boxes_detected[index][1],array_boxes_detected[index][0]),(array_boxes_detected[index][3],array_boxes_detected[index][2]),COLOR_GREEN,2)
                        # pass
                # Iterate over every possible 2 by 2 between the points combinations
            if len(transformed_downoids) >= 2:
                list_indexes = list(itertools.combinations(range(len(transformed_downoids)), 2))
                points = []
                for i,pair in enumerate(itertools.combinations(transformed_downoids, r=2)):
                    # Check if the distance between each combination of points is less than the minimum distance chosen
                    if math.sqrt( (pair[0][0] - pair[1][0])**2 + (pair[0][1] - pair[1][1])**2 ) < int(distance_minimum):
                        # Change the colors of the points that are too close from each other to red
                        if not (pair[0][0] > width or pair[0][0] < 0 or pair[0][1] > height+200  or pair[0][1] < 0 or pair[1][0] > width or pair[1][0] < 0 or pair[1][1] > height+200  or pair[1][1] < 0):
                            change_color_on_topview(pair)
                            # Get the equivalent indexes of these points in the original frame and change the color to red
                            index_pt1 = list_indexes[i][0]
                            index_pt2 = list_indexes[i][1]

                            if (frame_num % math.floor(frameRate) == 0):
                                current_sec = int(frame_num/math.floor(frameRate))
                                if not result.get(current_sec,None):
                                    result[current_sec] = []

                                box1 = [array_boxes_detected[index_pt1][1],
                                          array_boxes_detected[index_pt1][0],
                                          array_boxes_detected[index_pt1][3],
                                          array_boxes_detected[index_pt1][2]]

                                box2 = [array_boxes_detected[index_pt2][1],
                                         array_boxes_detected[index_pt2][0],
                                         array_boxes_detected[index_pt2][3],
                                         array_boxes_detected[index_pt2][2]]

                                if not any(d['bbox'] == box1 for d in result[current_sec]):
                                    result[current_sec].append({"class":"too_close","bbox":box1})

                                if not any(d['bbox'] == box2 for d in result[current_sec]):
                                    result[current_sec].append({"class": "too_close", "bbox": box2})






                                import json
                                with open('../output/result.json', 'w') as fp:
                                    json.dump(result, fp, indent=4)


                            cv2.rectangle(frame,(array_boxes_detected[index_pt1][1],array_boxes_detected[index_pt1][0]),(array_boxes_detected[index_pt1][3],array_boxes_detected[index_pt1][2]),COLOR_RED,2)
                            cv2.rectangle(frame,(array_boxes_detected[index_pt2][1],array_boxes_detected[index_pt2][0]),(array_boxes_detected[index_pt2][3],array_boxes_detected[index_pt2][2]),COLOR_RED,2)


                            cv2.imwrite("../output/final.jpg", frame)

        # Draw the green rectangle to delimitate the detection zone
        # draw_rectangle(corner_points)
        # Show both images
        # cv2.imshow("Bird view", bird_view_img)
        # cv2.imshow("Original picture", frame)
        frame_num = frame_num + 1
        key = cv2.waitKey(1) & 0xFF

        # Write the both outputs video to a local folders
        if output_video_1 is None and output_video_2 is None:
            fourcc1 = cv2.VideoWriter_fourcc(*"MJPG")
            output_video_1 = cv2.VideoWriter("../output/video.avi", fourcc1, 25,(frame.shape[1], frame.shape[0]), True)
            fourcc2 = cv2.VideoWriter_fourcc(*"MJPG")
            output_video_2 = cv2.VideoWriter("../output/bird_view.avi", fourcc2, 25,(bird_view_img.shape[1], bird_view_img.shape[0]), True)
        elif output_video_1 is not None and output_video_2 is not None:
            output_video_1.write(frame)
            output_video_2.write(bird_view_img)

        # Break the loop
        if key == ord("q"):
            break

get_social_distance("../video/kasim.mp4",60)
