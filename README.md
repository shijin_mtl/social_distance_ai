
# Installation


### Other requirements
All the other requirements can be installed via the command : 
```bash
pip install -r requirements.txt
```

### Start social distancing detection
Run 
```bash

from src.social_distanciation_video_detection import get_social_distance

get_social_distance(video_path="/video/PETS2009.avi",distance_minimum=50)

distance_minimum - minimum distance in px

```


# Outputs
Both video outputs (normal frame and bird eye view) will be stored in the outputs file.

output/video.avi - output video

output/bird_view.avi - output bird view video

output/result.json - output json file (each seconds detection values)



### Bird view functions

src/bird_view_tranfo_functions.py

compute_perspective_transform - create perspective transform of base image for bird eye view

compute_point_perspective_transformation - Apply the perspective transformation to every ground point which have been detected on the main frame







